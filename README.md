# DOCKER TEST #

Мое личное тестовое приложения для исследование докера

### Полезные ссылки ###

[http://geekyplatypus.com/dockerise-your-php-application-with-nginx-and-php7-fpm/](http://geekyplatypus.com/dockerise-your-php-application-with-nginx-and-php7-fpm/)

### Предварительная установка ###

* Docker ([https://download.docker.com/linux/ubuntu/dists/](https://download.docker.com/linux/ubuntu/dists/), выбираем нужную версию убунты, далее `pool/stable/amd64/`)
* Docker Composer ([https://github.com/docker/compose/releases](https://github.com/docker/compose/releases), следуем инструкции и выбираем версию под установленный Docker)

### Сделано ###

* Volume для backend теперь имеет корректные привилегии на запись
* Сделана подготовка для скейлинга приложения (on-fly скейлинг не поддерживается, так как это возможно только в Nginx Plus или сторонние модули)

### Запуск ###

В директории с проектом просто выполнить `docker-compose down && docker-compose up --build -d --scale php=5` (возможно потребуется через `sudo`, если текущий пользователь не добавлен в группу `docker`, [подробнее тут](https://docs.docker.com/engine/installation/linux/linux-postinstall/#manage-docker-as-a-non-root-user))