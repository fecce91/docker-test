<?php

exec('ip addr show', $output);

echo '<pre>';
print_r($output);
echo '</pre>';

echo '<br />';
echo 'Is writable `/var/www/storage`? ';
var_dump(is_writable('/var/www/storage'));

echo '<br />';
echo 'Is writable `/var/www/cache`? ';
var_dump(is_writable('/var/www/cache'));